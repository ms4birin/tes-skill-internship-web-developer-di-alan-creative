<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = "products";

    public $timestamps = true;

    protected $fillable = [
        'name', // Tambahkan atribut yang ingin diizinkan untuk pengisian massal di sini
        'price', // Contoh lain jika Anda memiliki atribut lain yang perlu diizinkan
    ];
}
